package org.coderfun.fieldmeta.dao;

import klg.j2ee.common.dataaccess.BaseRepository;
import org.coderfun.fieldmeta.entity.Tablemeta;

public interface TablemetaDAO extends BaseRepository<Tablemeta, Long> {

}
